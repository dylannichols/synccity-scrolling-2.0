
        $(document).ready(function () {

            var direction = "-100%"

            function switchDirection() {
                if (direction === "100%")
                    direction = "-100%";
                else
                    direction = "100%"
            }

            $("body").css("overflow", "hidden");

            $(".logo").click(function () {

                new TimelineMax()
                    .fromTo(".landing-main", 1, { y: "90%" }, { y: "200%", visibility: "hidden" })
                    .fromTo(".landing-two", 1, { x: "-100%" }, { x: "0%", opacity: 1 }, "-=0.5")
                    .to(".landing", 1, { height: "100vh" }, "-=1.5")
                    .to(".netflix-carousel", 1, {visibility: "hidden"}, "-=1.5")

            })

            $(".tv-link").click(function () {

                new TimelineMax()
                    .fromTo(".landing-two", 1, { x: "0%" }, { x: "-100%", opacity: 0 })
                    .to(".landing", 1, { height: "30vh"})
                    .to(".netflix-carousel", 1, {visibility: "visible"}, "-=1")

            })

            $(".platform-link").click(function () {

                new TimelineMax()
                    .fromTo(".landing-two", 1, { x: "0%" }, { x: "-100%", opacity: 0 })
                    .fromTo(".landing-main", 1, { y: "-200%" }, { y: "90%", visibility: "visible" });
            })

            $(".main-link").click(function () {

                new TimelineMax()
                    .to(".logo", 1, { opacity: 0 })
                    .fromTo(".landing-one", 1, { y: "0%" }, { y: "-100%", ease: Linear.easeNone, opacity: 0 })
                    .fromTo(".sections2", 1, { x: "100%" }, { x: "0%", ease: Linear.easeNone, display: 'block', opacity: 1 })
                    .to("nav, .section2", 1, { opacity: 1, display: "flex" })
                    .to(".material-icons", 1, { opacity: 0.3 }, "-=1")
                    .to("body", 1, { overflowY: "visible" }, "-=1");
            })

            $(".main-link2").click(function () {

                new TimelineMax()
                    .to(".logo", 1, { opacity: 0 })
                    .fromTo(".landing-one", 1, { y: "0%" }, { y: "-100%", ease: Linear.easeNone, opacity: 0 })
                    .fromTo(".sections", 1, { x: "100%" }, { x: "0%", ease: Linear.easeNone, display: 'block', opacity: 1 })
                    .to("nav, .section", 1, { opacity: 1, display: "flex" })
                    .to(".material-icons", 1, { opacity: 0.3 }, "-=1")
                    .to("body", 1, { overflowY: "visible" }, "-=1");
            })

            $(".home-link").click(function () {

                new TimelineMax()
                    .to("body", 1, { overflow: "hidden" })
                    .to("nav, .section2, .material-icons, .section", 1, { opacity: 0 }, "-=1")
                    .to(".sections2, .sections", 1, { opacity: 0, display: "none" })
                    .fromTo(".landing-one", 1, { y: "-100%" }, { y: "0%", ease: Linear.easeNone, opacity: 1 })
                    .to(".logo", 1, { opacity: 1 })

            })

            $(".sections2-link").click(function () {

                new TimelineMax()
                    .to("body", 1, { overflow: "hidden" })
                    .to(".section, .section2, nav, .material-icons", 1, { opacity: 0, display: 'none' }, "-=1")
                    .to(".sections, .sections2", 1, { opacity: 0, display: 'none' })
                    .fromTo(".sections2", 1, { x: direction }, { x: "0%", ease: Linear.easeNone, opacity: 1, display: 'block' })
                    .to("nav, .section2, .material-icons", 1, { opacity: 1, display: "flex" })
                    .to("body", 1, { overflowY: "visible" }, "-=1");

                switchDirection();
            })

            $(".sections-link").click(function () {

                new TimelineMax()
                    .to("body", 1, { overflow: "hidden" })
                    .to(".section2, .section, nav, .material-icons", 1, { opacity: 0, display: 'none' }, "-=1")
                    .to(".sections, .sections2", 1, { opacity: 0, display: 'none' })
                    .fromTo(".sections", 1, { x: direction }, { x: "0%", ease: Linear.easeNone, opacity: 1, display: 'block' })
                    .to("nav, .section, .material-icons", 1, { opacity: 1, display: "flex" })
                    .to("body", 1, { overflowY: "visible" }, "-=1");

                switchDirection();
            })

        });

        $(function () { // wait for document ready
            // init

            var controller = new ScrollMagic.Controller();
            // define movement of panels
            var wipeAnimation = new TimelineMax()
                .to(".section2", 0.5, { opacity: 1 }, "+=1")  // new content appears

                .fromTo(".section2.one", 2, { y: "0%" }, { y: "100%", ease: Linear.easeNone, display: "none" }, "+=2")    // first section leaving
                .fromTo(".section2.two", 1, { x: "-100%" }, { x: "0%", ease: Linear.easeNone, display: "flex" })          // second section enters      
                .fromTo(".section2.one2", 0.5, { x: "100%" }, { x: "0%", ease: Linear.easeNone, display: "flex" }, "+=2")   // other part of second section enters

                .fromTo(".section2.two", 2, { y: "0%" }, { y: "-100%", ease: Linear.easeNone, display: "none" }, "+=2")   // second section leave  
                .fromTo(".section2.one2", 0.5, { y: "0%" }, { y: "100%", ease: Linear.easeNone, display: "none" })   // other part leaves

                .fromTo(".section2.three", 1, { x: "100%" }, { x: "0%", ease: Linear.easeNone, display: "flex" })         // section three enters
                .fromTo(".section2.three", 0.5, { y: "0" }, { y: "100%", ease: Linear.easeNone, display: "none" }, "+=4")  // section three leaves
                .fromTo(".section2.four", 0.5, { y: "-100%" }, { y: "0%", ease: Linear.easeNone, display: "flex" })         // section four enters


            // create scene to pin and link animation
            new ScrollMagic.Scene({
                triggerElement: "#scrollContainer2",
                triggerHook: "onLeave",
                duration: "10000vh"
            })
                .setPin(".sections2")
                .setTween(wipeAnimation)
                .addTo(controller);

            var wipeAnimation2 = new TimelineMax()
                .fromTo(".section.one", 2, { y: "0%" }, { y: "100%", ease: Linear.easeNone, display: "none" }, "+=2")    // first section leaving

                .fromTo(".section.two", 1, { x: "-100%" }, { x: "0%", ease: Linear.easeNone, display: "flex" }, "+=2")          // second section enters      
                .fromTo(".section.two-two", 1, { x: "100%" }, { x: "0%", opacity: 1, visibility: "visible", ease: Linear.easeNone, display: "flex" }, "+=2")   // other part of second section enters
                .fromTo(".section.two-three", 1, { y: "100%" }, { y: "0%", ease: Linear.easeNone, visibility: "visible", display: "flex" }, "+=2")

                .fromTo(".section.two", 2, { y: "0%" }, { y: "-100%", ease: Linear.easeNone, display: "none" }, "+=4")   // second section leave  
                .fromTo(".section.two-three", 0.5, { y: "0%" }, { y: "100%", ease: Linear.easeNone, display: "none" })   // other part leaves
                .fromTo(".section.two-two", 0.5, { x: "0%" }, { x: "-100%", ease: Linear.easeNone, display: "none" }, "+=2")   // other part leaves

                .fromTo(".section.three", 1, { x: "100%" }, { x: "0%", ease: Linear.easeNone, display: "flex" })         // section three enters
                .fromTo(".section.three", 0.5, { y: "0" }, { y: "100%", ease: Linear.easeNone, display: "none" }, "+=4")  // section three leaves
                .fromTo(".section.four", 1, { y: "-100%" }, { y: "0%", ease: Linear.easeNone, display: "flex" })         // section four enters

            // create scene to pin and link animation
            new ScrollMagic.Scene({
                triggerElement: "#scrollContainer",
                triggerHook: "onLeave",
                duration: "10000vh"
            })
                .setPin(".sections")
                .setTween(wipeAnimation2)
                .addTo(controller);
        });

        $('.swiper-slide').hover(function () {
            $(this).removeClass('item-next').removeClass('item-prev').addClass('item-active');
            $(this).prevAll().removeClass('item-active').removeClass('item-next').addClass('item-prev');
            $(this).nextAll().removeClass('item-active').removeClass('item-prev').addClass('item-next');
        }, function () {
            $('.swiper-slide').removeClass('item-next').removeClass('item-prev').removeClass('item-active');
        })
